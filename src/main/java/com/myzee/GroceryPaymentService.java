package com.myzee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/grocery")
public class GroceryPaymentService {
	
	@Value("${server.port}")
	private String port;
	
	int sum = 0;
	@RequestMapping("/payment")
	public String payment(@RequestBody ItemsList itemList) {
		itemList.getItemList().stream().forEach(e -> {
			sum += e.getPrice();
			
		});
		return sum + "port: " + port;
	}
}
